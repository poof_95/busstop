package org.bataykin.service;

import lombok.Data;
import lombok.extern.java.Log;
import lombok.val;
import org.bataykin.model.BusService;
import org.bataykin.enums.CompanyType;
import org.bataykin.utils.BusServiceUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static java.nio.file.Paths.get;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.SPACE;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.bataykin.constant.Constants.GET_INPUT_FILE_MESSAGE;
import static org.bataykin.constant.Constants.GET_OUTPUT_FILE_MESSAGE;
import static org.bataykin.constant.Constants.NEW_LINE_CHARACTER;
import static org.bataykin.enums.CompanyType.GROTTY;
import static org.bataykin.enums.CompanyType.POSH;
import static org.bataykin.utils.BusServiceUtils.busServiceProcessing;

@Log
@Data
public class TimeTableService {
    private Path pathInputFile;
    private Path pathOutputFile;

    public TimeTableService() {
        val sc = new Scanner(System.in);

        log.info(GET_INPUT_FILE_MESSAGE);
        this.pathInputFile = get(sc.nextLine());

        log.info(GET_OUTPUT_FILE_MESSAGE);
        val outputFile = sc.nextLine();

        this.pathOutputFile = isEmpty(outputFile) ? null : get(outputFile);
    }

    public void createTimeTable() throws IOException {

        val busServiceCompanies = Files.lines(this.pathInputFile)
                .map(line -> line.split(SPACE))
                .map(BusService::new)
                .filter(BusServiceUtils::isLessHour)
                .sorted(comparing(BusService::getDepartureTime))
                .collect(toList());

        busServiceProcessing(busServiceCompanies);

        val timeTableMap = busServiceCompanies.stream()
                .sorted(comparing(BusService::getDepartureTime))
                .collect(groupingBy(BusService::getCompanyType));

        outputTimeTable(timeTableMap);
    }

    private void outputTimeTable(final Map<CompanyType, List<BusService>> timeTableMap) {
        val sb = new StringBuilder();

        timeTableMap.get(POSH).forEach(bs -> sb.append(bs).append(NEW_LINE_CHARACTER));
        sb.append(NEW_LINE_CHARACTER);
        timeTableMap.get(GROTTY).forEach(bs -> sb.append(bs).append(NEW_LINE_CHARACTER));

        if (isNull(pathOutputFile)) {
            log.info(sb.toString().trim());
        } else {
            try (val printer = new PrintWriter(new FileWriter(pathOutputFile.toString()))) {
                printer.print(sb.toString().trim());
            } catch (IOException e) {
                log.info(e.getMessage());
            }
        }
    }
}
