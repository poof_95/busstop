package org.bataykin.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.val;
import org.bataykin.model.BusService;

import java.util.List;

import static java.util.Objects.isNull;
import static org.bataykin.enums.CompanyType.POSH;
import static org.bataykin.enums.ServiceEfficiencyType.EFFECTIVE;
import static org.bataykin.enums.ServiceEfficiencyType.INEFFECTIVE;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BusServiceUtils {
    private static final int ONE_HOUR = 1;

    public static boolean isLessHour(final BusService busService) {
        return busService.getArrivalTime().minusHours(busService.getDepartureTime().getHour())
                .minusMinutes(busService.getDepartureTime().getMinute()).getHour() <= ONE_HOUR;
    }

    public static void busServiceProcessing(final List<BusService> busServices) {
        val bs1 = busServices.remove(0);
        val bs2 = busServices.stream()
                .filter(bs -> bs.getArrivalTime().equals(bs1.getArrivalTime()) || bs.getDepartureTime().equals(bs1.getDepartureTime()))
                .findFirst()
                .orElse(null);

        if (!isNull(bs2)) {
            busServices.remove(bs2);
        }
        busServices.add(getEffectiveBusService(bs1, bs2));

        if (busServices.stream().anyMatch(busService -> busService.getEfficiencyType() == INEFFECTIVE)) {
            busServiceProcessing(busServices);
        }
    }

    private static BusService getEffectiveBusService(final BusService bs1, final BusService bs2) {
        if (isNull(bs2)) {
            bs1.setEfficiencyType(EFFECTIVE);
            return bs1;
        }
        if (bs1.getDepartureTime().equals(bs2.getDepartureTime()) && bs1.getArrivalTime().equals(bs2.getArrivalTime()) &&
                bs1.getCompanyType() == POSH) {
            bs1.setEfficiencyType(EFFECTIVE);
            return bs1;
        }

        if ((bs1.getDepartureTime().equals(bs2.getDepartureTime()) || bs1.getDepartureTime().isAfter(bs2.getDepartureTime()))
                && bs1.getArrivalTime().isBefore(bs2.getArrivalTime())) {
            bs1.setEfficiencyType(EFFECTIVE);
            return bs1;
        }

        if (bs1.getDepartureTime().isAfter(bs2.getDepartureTime()) && bs1.getArrivalTime().isBefore(bs2.getArrivalTime())) {
            bs1.setEfficiencyType(EFFECTIVE);
            return bs1;
        }

        bs2.setEfficiencyType(EFFECTIVE);
        return bs2;
    }
}
