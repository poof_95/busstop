package org.bataykin.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    public static final String NEW_LINE_CHARACTER = "\n";

    public static final String GET_INPUT_FILE_MESSAGE = "Enter path to input file: ";

    public static final String GET_OUTPUT_FILE_MESSAGE = "Enter the path to the output file or leave it blank for output to the console: ";
}
