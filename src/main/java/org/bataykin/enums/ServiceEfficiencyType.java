package org.bataykin.enums;

public enum ServiceEfficiencyType {

    EFFECTIVE,
    INEFFECTIVE
}
