package org.bataykin;

import lombok.val;
import org.bataykin.service.TimeTableService;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        val timeTable = new TimeTableService();
        timeTable.createTimeTable();
    }
}
