package org.bataykin.model;

import lombok.Data;
import org.bataykin.enums.CompanyType;
import org.bataykin.enums.ServiceEfficiencyType;

import java.time.LocalTime;

import static java.lang.String.format;
import static java.time.LocalTime.parse;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.bataykin.enums.CompanyType.valueOf;
import static org.bataykin.enums.ServiceEfficiencyType.INEFFECTIVE;

@Data
public class BusService {

    private CompanyType companyType;
    private LocalTime departureTime;
    private LocalTime arrivalTime;
    private ServiceEfficiencyType efficiencyType;

    public BusService(final String name, final LocalTime departureTime, final LocalTime arrivalTime) {
        this.companyType = valueOf(name.toUpperCase());
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.efficiencyType = INEFFECTIVE;
    }

    public BusService(final String...args) {
        this(args[0], parse(args[1], ISO_LOCAL_TIME), parse(args[2], ISO_LOCAL_TIME));
    }

    @Override
    public String toString() {
        return format("%s %s %s", capitalize(companyType.name().toLowerCase()), departureTime, arrivalTime);
    }
}
